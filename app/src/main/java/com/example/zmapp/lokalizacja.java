package com.example.zmapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import static com.example.zmapp.R.id.button_poznan;
import static com.example.zmapp.R.id.button_gdansk;
import static com.example.zmapp.R.id.button_wawa;
import static com.example.zmapp.R.id.button_krakow;

public class lokalizacja extends AppCompatActivity {
    private Object intent;
    private String lokalizacja;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lokalizacja);
    }



        public void Click_Specjalizacja(View view) {


            switch (view.getId())
            {

                case button_poznan: lokalizacja = "Poznań"; break;
                case button_gdansk: lokalizacja = "Gdańsk"; break;
                case button_wawa: lokalizacja = "Warszawa"; break;
                case button_krakow: lokalizacja = "Kraków"; break;

            }
            Intent intent = new Intent();
            intent = new Intent(lokalizacja.this,  thankyoupage.class);
            intent.putExtra("com.example.zmapp.MESSAGE", lokalizacja);
            startActivity(intent);

        }
}